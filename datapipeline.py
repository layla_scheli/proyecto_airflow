import pandas as pd
from google.cloud import bigquery
# 2. Run
# gcloud composer environments update composer-datahack --update-pypi-packages-from-file requirements.txt --location us-central1 
def readStorage():
    df_01 = pd.read_csv("gs://python-files-datahack/data/retail-data/by-day/2010-12-01.csv")
    transformData(df_01)

def transformData(df_01):
    # Calculamos la nueva variable
    df_01["Total01"] = df_01["Quantity"]*df_01["UnitPrice"]
    # Agrupamos y generamos el nuevo dataframe
    df_01_agg = pd.DataFrame(df_01.groupby(["Description"])["Total01"].agg(sum)).reset_index()
    df_01_agg.to_csv("gs://python-files-datahack/data/tmp/enrichedData_layla.csv", index = False )

def writeBq():
    df = pd.read_csv("gs://python-files-datahack/data/tmp/enrichedData_layla.csv")

    table_id = 'fact_project.upsales_byday_layla'
    # Since string columns use the "object" dtype, pass in a (partial) schema to ensure the correct BigQuery data type.
    client = bigquery.Client()

    job_config = bigquery.LoadJobConfig(schema=[
        bigquery.SchemaField("Description", "STRING"),
        bigquery.SchemaField("Total01", "FLOAT")
    ])

    job = client.load_table_from_dataframe(
        df, table_id, job_config=job_config
    )

    job.result()