from airflow import DAG
from airflow.utils.dates import days_ago

from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from airflow.utils.helpers import chain, cross_downstream
#chain many operators and single direction
#

from random import seed, random

default_arguments = {
    'owner': 'Layla Scheli',
    'start_date': days_ago(1)
}

#/home/hola/airflow-tutorial/dags
#catchup by default is true, keep true if you need to run from the past
#dag = DAG('core_concepts', schedule_interval='@daily', catchup=False)
#to avoid dag=dag

with DAG(
    dag_id: 'core_concepts_layla', 
    schedule_interval='@daily', 
    catchup=False,
    default_args=default_arguments,
) as dag:

    bash_task = BashOperator(
        task_id="bash_command", 
        bash_command='echo "La fecha de hoy es: $TODAY"', 
        env={'TODAY': "2021-06-21"},
    )

    def print_random_number(number):
        for _ in range(10):
            seed(number)
            print(random())
    
    python_task = PythonOperator(
        task_id='python_function', 
        python_callable=print_random_number, 
        op_args=[1],
    )

bash_task >> python_task